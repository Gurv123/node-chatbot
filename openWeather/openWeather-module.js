let queryString = require('querystring');

class OpenWeatherMap{
  constructor(APPID , units) {
    this._APPID = APPID;
    this._units = units;
  }

  async getWeather(city){
    const data = {q:city, APPID: this._APPID, units: this._units}
    const url = "https://api.openweathermap.org/data/2.5/weather?" + queryString.stringify(data)

    console.log(url);
    let response = await fetch(url);
    return await response.json()
  }
};

module.exports.OpenWeatherMap = OpenWeatherMap;