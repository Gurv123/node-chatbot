const readline = require('readline');
/* 'gambit' renvoie a la notion de tactique, d'entree en matiere, 
 * d'amorce de conversation 
 * il s'agit d'un tableau, contenant des object literaux.
 * chaque objet contient deux cles, 'trigger' correspondant 
 * a un expression reguliere
 * et output contenant la/les reponse(s) possible(s), ou une fonction 
 * dont le role est de retourner la reponse
 *
 *  @todo : ajouter plusieurs autres possibilites ( dont la date )
 *  @todo (avance): ajouter la possibilite de gerer des functions 
 *      dans les outputs :
 * 	 { 'trigger' : /(?:.*)temps a?)(?:.*)/,
 *	  'output'  : ['^getWeather']
 *	 },
 *  @todo (avanc"): trouver une API et l'interroger ( ex: meteo ). 
 *       Utiliser le module 'sync-request'. */
const gambits = [
	{ 
		'trigger' : /salut|bonjour|hello|hi\s*(.*)/,
	  'output'  : ['Salut!', 'Bonjour', 'Yo !']
	},
	{ 
		'trigger' : /(comment\s?)?[c]a\sva/,
	  'output'  : ['Je vais bien merci', 'Très bien et vous ?']
	},
	{ 
		'trigger' : /date|jour/,
	  'output'  : ['^getDate']
	},
	{
		'trigger': /(?:météo|meteo|quel temps à ?)\s(.+)/,
		'output':['^getWeather']
	},
	{ 
		'trigger' : /(.*[0-9])[*](.*[0-9])/,
	  'output'  : ['^getMultiplication']
	},
	{ 
		'trigger' : /(.*[0-9])[\/](.*[0-9])/,
	  'output'  : ['^getDivision']
	},
	{ 
		'trigger' : /(.*[0-9])[+| + ](.*[0-9])/,
	  'output'  : ['^getAddition']
	},
	{ 
		'trigger' : /(.*[0-9])[-](.*[0-9])/,
	  'output'  : ['^getSoustraction']
	},
	{ 
		'trigger' : /(.*[0-9])[^](.*[0-9])/,
	  'output'  : ['^getPuissance']
	},
	{ 
		'trigger' : /(.*[0-9])[²]/,
	  'output'  : ['^getSquare']
	},
];

let { OpenWeatherMap } = require('./openWeather/openWeather-module.js');
const { API_KEY } = require('./openWeather/ow-api-key.js');
let ow = new OpenWeatherMap(API_KEY, 'metric', {});

// Utilitaires
let utils = {
	random: function (min, max) {
  	return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	getDate: function() {
		return new Date().toLocaleString() + '\n';
	},
	getWeather: async (result) => {
		const city = result[1];
		let res = await ow.getWeather(city);
		let meteo = await res.main.temp;
		return city + ': ' + meteo + '°\n';
	},
	getMultiplication: function (result) {
		const x = parseFloat(result[1]);
		const y = parseFloat(result[2]);
		return (x * y) + '\n';
	},
	getDivision: function (result) {
		const x = parseFloat(result[1]);
		const y = parseFloat(result[2]);
		return (x / y) + '\n';
	},
	getAddition: function (result) {
		const x = parseFloat(result[1]);
		const y = parseFloat(result[2]);
		return (x + y) + '\n';
	},
	getSoustraction: function (result) {
		const x = parseFloat(result[1]);
		const y = parseFloat(result[2]);
		return (x - y) + '\n';
	},
	getSquare: function (result) {
		const x = parseFloat(result[1]);
		return (x * x) + '\n';
	},
	getPuissance: function (result) {
		const x = parseFloat(result[1]);
		const y = parseFloat(result[2]);
		return (Math.pow(x, y)) + '\n';
	}
}


//notez que l'on specifie un stream en entree et en sortie.

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// ceci n'est pas obligatoire, permet de poser une question et 
// d'attendre une reponse de l'utilisateur
rl.question('Salut, c\'est Bobby ! Votre ChatBot préféré !\nQuel est votre prenom ?\n', function(answer) {
	console.log('Bienvenue ' + answer + ' ! \n');
});
// L'evenement 'ligne' est emis chaque fois que le flux d'entree recoit
// une entree de fin de ligne (\ n, \ r ou \ r \ n). 
// Cela se produit generalement lorsque l'utilisateur appuie sur 
//les touches <Entree> ou <Retour>.
rl.on('line', async function(input){
	let found = false;
	// let len   = gambits.length;

	// iteration sur chaque gambit afin de touver une correspondance 
	// avec ce qui a ete saisi
	for (gambit of gambits) {
		//@todo: ecrire la logique qui permet de renvoyer *la premiere reponse* 
		//correspondant a l'input reconnu.
		//@todo (avance): ecrire une fonction qui permet de renvoyer 
		// aleatoirement une des reponses possibles
		let result = input.match(gambit.trigger);
		
		if (result != null) {
			let index = utils.random(0, gambit['output'].length - 1);
			let output = gambit['output'][index];
			found = true;

			if (output[0] == '^') {
				let func = output.substring(1);
				// rl.oause();
				let res = await utils[func](result);
				if (res != 'undefined') {console.log('>', res); }
			} else {
				console.log(output + '\n')
			}
			break;
		}
	}
	if(!found){
		console.log("Je n'ai pas compris \n");
	}
});